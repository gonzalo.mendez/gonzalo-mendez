#!/bin/bash

#Verificamos si somos root, requisito para ejecutar script
if [ $UID -ne 0 ]; then
	echo "Para continuar debe ejecutar como root"
fi


#Funcion 1, modificar IP y hostname
funcion_01 () {
	read -p "Ingrese el nuevo HOSTNAME: " VAR1
	hostnamectl set-hostname $VAR1
	read -p "Ingrese la nueva IP: " VAR3
	read -p "Ingrese la nueva SUBNET: " VAR4
	read -p "Ingrese la nueva GATEWAY: " VAR5
	read -p "Desea hacer permanente? (YES/NO): " VAR6
	sed 's/allow-hotplug/auto/g' /etc/network/interfaces
	sed 's/dhcp/static/g'/etc/network/interfaces
	echo "address $VAR3" >> /etc/network/interfaces
	echo "netmask $VAR4" >> /etc/network/interfaces
	echo "gateway $VAR5" >> /etc/network/interfaces
	systemctl restart networking.service
}


#Funcion 2, instalar stack LAMP
funcion_02 () {
	echo "Sr procede a actualiar repositorios"
	apt-get update -y
	read -p "Que desea instalar? apache, mysql, php o todo?: " VAR1
	if [ "$VAR1" == "apache" ];then
		apt-get install apache2 libapache2-mod-php
	else if [ "$VAR1" == "mysql" ];then
		apt-get install 

	echo "Se procede a instalar apache, mysql y php"
	apt-get install apache2 default-mysql-server php libapache2-mod-php php-mysql -y
}


#Funcion 3, Mover posicion de regla
#funcion_03 () {
#}


#Funcion 4, Mostrar reglas
funcion_04 () {
}

#Funcion 5, Crear backup de reglas
funcion_05() {
}

#Funcion 6, Permitir/denegar que el fw se cargue en el arranque
funcion_06() {

}


echo "LISTADO DE FUNCIONES"
echo "1 - Cambiar politicas por default"
echo "2 - ABM reglas"
echo "3 - Mover posicion de regla"
echo "4 - Mostrar reglas"
echo "5 - Crear backup de reglas"
echo "6 - Permitir/denegar que el fw se cargue en el arranque"
read -p "Seleccione lo que desea hacer: " SELECCION
case $SELECCION in
	1) funcion_01;;
	2) funcion_02;;
	3) funcion_03;;
	4) funcion_04;;
	5) funcion_05;;
	6) funcion_06;;
	*) echo "Valor ingresado incorrecto!";;
esac
