#!/bin/bash

#Verificamos si somos root, requisito para ejecutar script
if [ $UID -ne 0 ]; then
	echo "Para continuar debe ejecutar como root"
fi


#Funcion 1, Cambiar politicas por default
funcion_01 () {
	echo "Vamos a cambiar las polity default (escriba como figuran las palabras en mayuscula)"
	read -p "Quiere permitir (ACCEPT) o denegar (DROP)? " VAR1
	read -p "Que cadena desea cambiar? INPUT, OUTPUT, FORWARD? " VAR2
	if [ "$VAR1" = "ACCEPT" ];then
		if [ "$VAR2" = "FORWARD" ]; then
			iptables -P $VAR2 $VAR1
			iptables -L 
		elif [ "$VAR2" = "OUTPUT" ];then
			iptables -P $VAR2 $VAR1
			iptables -L 
		else
			iptables -P $VAR2 $VAR1
			iptables -L 
		fi	
	elif [ "$VAR1" = "DROP" ];then
		if [ "$VAR2" = "FORWARD" ]; then
			iptables -P $VAR2 $VAR1
			iptables -L 
		elif [ "$VAR2" = "OUTPUT" ];then
			iptables -P $VAR2 $VAR1
			iptables -L 
		else
			iptables -P $VAR2 $VAR1
			iptables -L
		fi
	fi
}


#Funcion 2, ABM reglas
#funcion_02 () {
#}


#Funcion 3, Mover posicion de regla
#funcion_03 () {
#}


Funcion 4, Mostrar reglas
funcion_04 () {
	echo "Listamos las reglas de la tabla NAT"
	iptables -L -t nat
	echo "Listamos las reglas de la tabla FILTER"
	iptables -L -t filter
	echo "Listamos las reglas de la tabla MANGLE"
	iptables -L -t mangle
}

Funcion 5, Crear backup de reglas
funcion_05() {
	read -p "Ingrese path completo donde quiere guardar las reglas: " VAR1
	iptables-save > $VAR1
}

#Funcion 6, Permitir/denegar que el fw se cargue en el arranque
#funcion_06() {
#}


echo "LISTADO DE FUNCIONES"
echo "1 - Cambiar politicas por default"
echo "2 - ABM reglas"
echo "3 - Mover posicion de regla"
echo "4 - Mostrar reglas"
echo "5 - Crear backup de reglas"
echo "6 - Permitir/denegar que el fw se cargue en el arranque"
read -p "Seleccione lo que desea hacer: " SELECCION
case $SELECCION in
	1) funcion_01;;
	2) funcion_02;;
	3) funcion_03;;
	4) funcion_04;;
	5) funcion_05;;
	*) echo "Valor ingresado incorrecto!";;
esac
