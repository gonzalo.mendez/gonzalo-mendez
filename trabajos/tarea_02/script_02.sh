#!/bin/bash

#Para ejecutar este script ejecutar como root

#Se define las variables a utilizar
VAR_LOGIN_ALL=/tmp/users_logins.log
VAR_DIR_LOGIN=/tmp/login
export VAR_TIMSTAMP=$(echo "$(date +%Y%m%d) - Inicio de sesion de $USER")

#Creo archivo donde almacenan todos los logins
touch $VAR_LOGIN_ALL

#Creo el directorio donde se almacena cada fichero por usuario
mkdir $VAR_DIR_LOGIN

#Se da permisos a ambos
chmod 777 $VAR_LOGIN_ALL $VAR_DIR_LOGIN

#Se agregan las lineas en el archivo /etc/bash.bashrc
echo 'echo "$(date +%Y%m%d%H%m%S) - Inicio de sesion de $USER" >> /tmp/users_logins.log' >> /etc/bash.bashrc
echo 'echo "$(date +%Y%m%d%H%m%S) - Inicio de sesion de $USER" >> /tmp/login/$USER.log' >> /etc/bash.bashrc
echo 'export VAR_TIMESTAMP=$(echo "$(date +%Y%m%d%H%m%S) - Inicio de sesion de $USER")' >> /etc/bash.bashrc

