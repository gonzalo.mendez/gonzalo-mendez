#!/bin/bash

#Se define las variables a utilizar
VAR_FOLDER=/backups/
VAR_LOG=/backups/backups.log

#Usuario del sistema $USER, los otros usuarios serian comunes, por lo cual se decide recorrer el /home/ y comprimir cada directorio

for i in $(ls /home)
do
	echo "$(date) - Comenzando backup de $i" >> $VAR_LOG
	#tar czf $VAR_FOLDER/$i-$(date +%Y%m%d).tar.gz /home/$i/ 2>&1 >> $VAR_LOG
	tar cvzf $VAR_FOLDER/$i-$(date +%Y%m%d).tar.gz --absolute-name /home/$i/ 2>&1 >> $VAR_LOG
	echo "$(date) - Finalizando backup de $i" >> $VAR_LOG
done

