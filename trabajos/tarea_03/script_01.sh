#!/bin/bash

#Verificamos si somos root, requisito para ejecutar script
if [ $UID -ne 0 ]; then
	echo "Para continuar debe ejecutar como root"
fi


#Funcion 1, cambio de permisos
funcion_01 () {
	read -p "Ingrese directorio o archivo a cambiar los permisos " VAR1
	if [ -d "$VAR1" ];then
		echo "LISTA DE PERMISOS"
		echo "0: Sin permisos"
		echo "1: Ejecución"
		echo "2: Escritura"
		echo "3: Lectura y escritura"
		echo "4: Lectura"
		echo "5: Lectura y ejecución"
		echo "6: Lectura y escritura"
		echo "7: Lectura, escritura y ejecución"
		read -p "Ingrese permiso para el usuario: " NUM1
		read -p "Ingrese permiso para el grupo: " NUM2
		read -p "Ingrese permiso para otros: " NUM3
		#read -p "Ingrese los permisos de manera Octal " VAR2
		chmod $NUM1$NUM2$NUM3 $VAR1
		ls -l $VAR1
	elif [ -f $VAR1 ];then
		echo "LISTA DE PERMISOS"
                echo "0: Sin permisos"
                echo "1: Ejecución"
                echo "2: Escritura"
                echo "3: Lectura y escritura"
                echo "4: Lectura"
                echo "5: Lectura y ejecución"
                echo "6: Lectura y escritura"
                echo "7: Lectura, escritura y ejecución"
                read -p "Ingrese permiso para el usuario: " NUM1
		read -p "Ingrese permiso para el grupo: " NUM2
                read -p "Ingrese permiso para otros: " NUM3
		#read -p "Ingrese los permisos de manera Octal " VAR2
			if [[ $NUM1 -le 7 && $NUM1 -ge 0 ]] ;then	
				if [[ $NUM2 -le 7 && $NUM2 -ge 0 ]] ;then
					if [[ $NUM3 -le 7 && $NUM3 -ge 0 ]] ;then
						chmod $NUM1$NUM2$NUM3 $VAR1
						ls -l $VAR1
					else 
						echo "Permisos ingresados incorrectos"
					fi
				else
					echo "Permisos ingresados incorrectos"
				fi
			else
				echo "Permisos ingresados incorrectos"
			fi
				
	else
		echo "El directorio o archivo ingresado no existe"
	fi
}


#Funcion 2, cambio de propiertario/owner
funcion_02 () {
	read -p "Ingrese directorio o archivo a cambiar el propietario " VAR1
        if [ -d $VAR1 ];then
		read -p "Ingrese el nuevo usuario propietario " VAR2
        	if [ id -u "$VAR2" >/dev/null 2>&1 ]; then
			read -p "Ingrese el nuevo grupo propietario " VAR3
			if [ id -g "$VAR3" >/dev/null 2>&1 ];then
				chown $VAR2:$VAR3 $VAR1
				ls -l $VAR1
			else
				echo "No existe el grupo en el sistema"
			fi
		else
			echo "No existe el usuario en el sistema"
		fi
	elif [ -f $VAR1 ];then
		read -p "Ingrese el nuevo usuario propietario " VAR2
                getent passwd $VAR2 > /dev/null
		if [ $? -eq 0 ]; then
                        echo $VAR2
			read -p "Ingrese el nuevo grupo propietario " VAR3
			getent group $VAR3 > /dev/null
                        if [ $? -eq 0 ];then
                                chown $VAR2:$VAR3 $VAR1
                                ls -l $VAR1
                        else
                                echo "No existe el grupo en el sistema"
                        fi
                else
                        echo "No existe el usuario en el sistema"
                fi
	else 
		echo "El archivo o directorio no existe"
	fi
}


#Funcion 3, crear y eliminar grupos
funcion_03 () {
	read -p "Que desea hacer, crear (ingrese 0) o eliminar (ingrese 1) grupo? " VAR1	
	if [ $VAR1 -eq 0 ];then
		read -p "Ingrese el nombre de grupo a crear " VAR2
		su - -c "groupadd $VAR2"
		getent group | grep $VAR2
	elif [ $VAR1 -eq 1 ];then
		read -p "Ingrese el nombre de grupo a borrar " VAR3
		su - -c "groupdel $VAR3"
		getent group | grep $VAR3	
	else
		echo "El valor que ingreso es incorrecto"
	fi
}


#Funcion 4, listar grupos existentes, existen dos formas, leyendo el archivo o por comando
funcion_04 () {
	#getent group
	cat /etc/group
}


#Funcion 4, buscar archivos y directorios segun permisos definidos por el usuario.
funcion_05() {
	read -p "Ingrese los permisos a buscar de manera octal (ej 777): " VAR1
	read -p "Ingrese la ubicación desde donde desea buscar: " VAR2	
	find  $VAR2 -perm $VAR1 -exec ls -l {} \; 
	
}


echo "LISTADO DE FUNCIONES"
echo "1 - Cambie los permisos rwx de archivos y direcorios para los tres tipos: 'User', 'Group' y 'Others', según el conjunto de permisos que defina el usuario"
echo "2 - Permita cambiar el dueño y grupo de ficheros y directorios definidos por el usuario"
echo "3 - Permita crear y eliminar grupos definidos por el usuario"
echo "4 - Liste grupos existentes"
echo "5 - Buscar fichero y directorios según un conjunto de permisos definididos por el usuario"
read -p "Seleccione lo que desea hacer: " SELECCION
case $SELECCION in
	1) funcion_01;;
	2) funcion_02;;
	3) funcion_03;;
	4) funcion_04;;
	5) funcion_05;;
	*) echo "Valor ingresado incorrecto!";;
esac
echo  "Exit status del script" $?
